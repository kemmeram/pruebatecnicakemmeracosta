<?php

namespace App\Modulos\Vehiculo;

use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{

    /**
     * The name of the table.
     *
     * @var string
     */
    protected $table = 'tbl_vehiculo';

    /**
     * The name of the primary key.
     *
     * @var string
     */
    protected $primaryKey = 'i_pk_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vc_placa', 'vc_color', 'vc_marca','vc_tipoDeVehiculo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];


    public function usuarios(){
        return $this->belongsToMany('\App\Modulos\Usuarios\Users','tbl_usuario_vehiculo_pivot','i_fk_id_vehiculo','i_fk_id_users');
    }

}
