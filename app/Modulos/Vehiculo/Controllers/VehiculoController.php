<?php

namespace App\Modulos\Vehiculo\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Modulos\Vehiculo\Vehiculo;
use App\Modulos\Usuarios\Users;
use Validator;
use Illuminate\Http\Request;


class VehiculoController extends Controller {


	public function index()
	{
        $vehiculos = Vehiculo::all();
        $usuarios = Users::all();
        $data = [
            'vehiculos' => $vehiculos,
            'propietarios' => $usuarios->where('i_fk_id_tipo',1),
            'conductores' => $usuarios->where('i_fk_id_tipo',2),
        ];

        return view('vehiculos.vehiculo',$data);
    }
    
    public function create_vehiculo(Request $request){

        //dd($request->all());
		$messages = [
		    'txt_placa.required'    => 'El campo Placa se encuentra vacio.',
		    'txt_color.required'    => 'El campo Color se encuentra vacio.',
		    'txt_marca.required' => 'El campo Marca se encuentra vacio.',
            'txt_tipoVeh.required'      => 'El campo Tipo de vehículo se encuentra vacio.',
            'selectPropietarios.required'      => 'El campo Propietario se encuentra vacio.',
            'selectConductores.required'      => 'El campo Conductores de vehículo se encuentra vacio.',
		    
		];

		$validator = Validator::make($request->all(),
		    [
				'txt_placa' => 'required',
				'txt_color' => 'required',
				'txt_marca' => 'required',
                'txt_tipoVeh' => 'required',
                'selectPropietarios'=> 'required',
                'selectConductores'=> 'required'
                
        	],$messages);
        

        if ($validator->fails())
            return response()->json(array('status' => 'error', 'errors' => $validator->errors()));
        
           if($request->input('id') == '0'){
                return $this->crear_vehiculo($request->all());
            }
            else{
                return $this->modificar_vehiculo($request->all());  
            }
    }

    public function crear_vehiculo($input)
    {
       //dd($input);
        $veh = new Vehiculo;
        $veh['vc_placa']=$input['txt_placa'];
		$veh['vc_color']=$input['txt_color'];
		$veh['vc_marca']=$input['txt_marca'];
		$veh['vc_tipoDeVehiculo']=$input['txt_tipoVeh'];
        $veh->save();
        
        $veh->usuarios()->attach($input['selectPropietarios']);

        foreach ($input['selectConductores'] as $key => $value) {
            $veh->usuarios()->attach($value);
        }

        return response()->json(array('status' => 'creado', 'datos' => $veh));
    }
    
    public function modificar_vehiculo($input)
    {
        $veh = Vehiculo::find($input["id"]);
        $veh['vc_placa']=$input['txt_placa'];
		$veh['vc_color']=$input['txt_color'];
		$veh['vc_marca']=$input['txt_marca'];
		$veh['vc_tipoDeVehiculo']=$input['txt_tipoVeh'];
		$veh->save();

        return response()->json(array('status' => 'modificado', 'datos' => $veh));
    }



}
