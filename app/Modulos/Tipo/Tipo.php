<?php

namespace App\Modulos\Tipo;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{


    /**
     * The name of the table.
     *
     * @var string
     */
    protected $table = 'tbl_tipo';

    /**
     * The name of the primary key.
     *
     * @var string
     */
    protected $primaryKey = 'i_pk_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vc_tipo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];
}
