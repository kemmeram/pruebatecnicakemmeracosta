<?php

namespace App\Modulos\Reportes\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Modulos\Vehiculo\Vehiculo;

class InformeController extends Controller {


	public function index()
	{
     
        $vehiculos = Vehiculo::with('usuarios')->get();

        $informacion = [];

        foreach ($vehiculos as $key => $vehiculo) {
            $informacion []= [
                'id' => $vehiculo->i_pk_id,
                'placa' => $vehiculo->vc_placa,
                'marca' => $vehiculo->vc_marca,
                'propietario' => $vehiculo->usuarios->where('i_fk_id_tipo',1)->first(),
                'conductores' => $vehiculo->usuarios->where('i_fk_id_tipo',2),
            ];
        }
        
        $data = [
            'datos' => $informacion
        ];       

        return view('informes.informe',$data );
	}


}
