<?php

namespace App\Modulos\Usuarios\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Modulos\Usuarios\Users;
use Validator;
use App\Modulos\Tipo\Tipo;

class UsuarioController extends Controller {


	public function index()
	{
        
        $usuarios = Users::all();
        $tipos = Tipo::all();
        $data = [
            'usuarios' => $usuarios,
            'tipos' => $tipos,
        ];

        return view('usuarios.usuario',$data);
    }

    public function crear(Request $request){

        //dd($request->all());
		$messages = [
		    'numCedula.required'    => 'El campo Numero de Cedula se encuentra vacio.',
		    'fNombre.required'    => 'El campo Primer Nombre se encuentra vacio.',
		    'tNombre.required' => 'El campo Segundo Nombre se encuentra vacio.',
		    'inp_apellidos.required'      => 'El campo Apellidos se encuentra vacio.',
		    'direc.required'      => 'El campo Direccion se encuentra vacio.',
		    'phone.required'      => 'El campo Telefono se encuentra vacio.',
		    'city.required'      => 'El campo Ciudad se encuentra vacio.',
		    'mail.required'      => 'El campo email se encuentra vacio.',
		    'selectTipo.required'      => 'El campo Tipo de Usuario se encuentra vacio.',
		];

		$validator = Validator::make($request->all(),
		    [
				'numCedula' => 'required',
				'fNombre' => 'required',
				'tNombre' => 'required',
				'inp_apellidos' => 'required',
				'direc' => 'required',
				'phone' => 'required',
				'city' => 'required',
				'mail' => 'required',
				'selectTipo' => 'required',
        	],$messages);
        

        if ($validator->fails())
            return response()->json(array('status' => 'error', 'errors' => $validator->errors()));
        
           if($request->input('id') == '0'){
                return $this->crear_usuario($request->all());
            }
            else{
                return $this->modificar_usuario($request->all());  
            }
    }

    public function crear_usuario($input)
    {
        
        $user = new Users;
        $user['vc_numeroCedula']=$input['numCedula'];
		$user['name']=$input['fNombre'];
		$user['vc_segundoNombre']=$input['tNombre'];
		$user['vc_apellidos']=$input['inp_apellidos'];
		$user['vc_direccion']=$input['direc'];
		$user['vc_telefono']=$input['phone'];
		$user['vc_ciudad']=$input['city'];
        $user['email']=$input['mail'];
        $user['password']=$input['numCedula'];
		$user['i_fk_id_tipo']=$input['selectTipo'];
		$user->save();

        return response()->json(array('status' => 'creado', 'datos' => $user));
    }
    
    public function modificar_usuario($input)
    {
        $user = Users::find($input["id"]);
        $user['vc_numeroCedula']=$input['numCedula'];
		$user['name']=$input['fNombre'];
		$user['vc_segundoNombre']=$input['tNombre'];
		$user['vc_apellidos']=$input['inp_apellidos'];
		$user['vc_direccion']=$input['direc'];
		$user['vc_telefono']=$input['phone'];
		$user['vc_ciudad']=$input['city'];
        $user['email']=$input['mail'];
        $user['password']=$input['numCedula'];
		$user['i_fk_id_tipo']=$input['selectTipo'];
		$user->save();

        return response()->json(array('status' => 'modificado', 'datos' => $user));
    }


}
