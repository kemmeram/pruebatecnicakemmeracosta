<?php

namespace App\Modulos\Usuarios;

use Illuminate\Database\Eloquent\Model;


class Users extends Model
{

    /**
     * The name of the table.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The name of the primary key.
     *
     * @var string
     */
    protected $primaryKey = 'i_pk_id';
    protected $appends = ['FullName'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vc_numeroCedula', 'name', 'vc_segundoNombre','vc_apellidos','vc_direccion','vc_telefono','vc_ciudad','email','email_verified_at','password','i_fk_id_tipo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    

    public function autos(){
        return $this->belongsToMany('\App\Modulos\Vehiculo\Vehiculo','tbl_usuario_vehiculo_pivot','i_fk_id_users','i_fk_id_vehiculo');
    }

    public function getFullNameAttribute()
    {
        return trim($this->name) . ' ' . trim($this->vc_segundoNombre) . ' ' . trim($this->vc_apellidos);
    }

}
