<?php

use Illuminate\Database\Seeder;

class TipoSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_tipo')->insert([
        
            [   
                'i_pk_id'=>1, 
                'vc_tipo'=>"Propietario", 
            ],
            [   
                'i_pk_id'=>2, 
                'vc_tipo'=>"Conductor", 
            ]
            ]);

    }
}