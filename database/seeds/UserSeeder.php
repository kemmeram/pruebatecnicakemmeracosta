<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        
            [   
                'i_pk_id'=>1, 
                'vc_numeroCedula'=>"123456",
                'name'=>"Luis",
                'vc_segundoNombre'=>"Fernando",
                'vc_apellidos'=>"Urrego Sanchez",
                'vc_direccion'=>"Calle 2 N 3-46",
                'vc_telefono'=>"3024567890",
                'vc_ciudad'=>"Bogota",
                'email'=>"propietario1@app.com",
                'password'=>"1234",
                'i_fk_id_tipo'=>"1",
                
            ],
            [   
                'i_pk_id'=>2, 
                'vc_numeroCedula'=>"9876543",
                'name'=>"Daniel",
                'vc_segundoNombre'=>"Andres",
                'vc_apellidos'=>"Ramirez Ronco",
                'vc_direccion'=>"Calle 6 N 4-16",
                'vc_telefono'=>"30000000000",
                'vc_ciudad'=>"Madrid",
                'email'=>"propietario2@app.com",
                'password'=>"123456",
                'i_fk_id_tipo'=>"1",
            ],
            [   
                'i_pk_id'=>3, 
                'vc_numeroCedula'=>"9876543",
                'name'=>"Martin",
                'vc_segundoNombre'=>"Felipe",
                'vc_apellidos'=>"Lugo Perez",
                'vc_direccion'=>"Calle 5 N 3-32",
                'vc_telefono'=>"3000000234",
                'vc_ciudad'=>"Sopo",
                'email'=>"conductor1@app.com",
                'password'=>"123abc",
                'i_fk_id_tipo'=>"2",
            ],
            [   
                'i_pk_id'=>4, 
                'vc_numeroCedula'=>"98745323",
                'name'=>"Camilo",
                'vc_segundoNombre'=>"Andres",
                'vc_apellidos'=>"Aguas Moscoso",
                'vc_direccion'=>"Calle 3 N 4-10",
                'vc_telefono'=>"300123456",
                'vc_ciudad'=>"Siberia",
                'email'=>"conductor2@app.com",
                'password'=>"abc123",
                'i_fk_id_tipo'=>"2",
            ]
            ]);
    }
}
