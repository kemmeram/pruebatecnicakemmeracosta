<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TipoSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(VehiculoSeeder::class);
    
    }
}