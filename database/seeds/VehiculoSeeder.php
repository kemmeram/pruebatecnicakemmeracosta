<?php

use Illuminate\Database\Seeder;

class VehiculoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_vehiculo')->insert([
        
            [   
                'i_pk_id'=>1, 
                'vc_placa'=>"CEH-123",
                'vc_color'=>"rojo",
                'vc_marca'=>"Toyota",
                'vc_tipoDeVehiculo'=>"Publico",
                
                
            ],
            [   
                'i_pk_id'=>2, 
                'vc_placa'=>"ABC-345",
                'vc_color'=>"azul",
                'vc_marca'=>"Kenworth",
                'vc_tipoDeVehiculo'=>"Particular",
            ],
            [   
                'i_pk_id'=>3, 
                'vc_placa'=>"JKA-745",
                'vc_color'=>"Verde",
                'vc_marca'=>"Kenworth",
                'vc_tipoDeVehiculo'=>"Publico",
            ],
            [   
                'i_pk_id'=>4, 
                'vc_placa'=>"CHN-654",
                'vc_color'=>"AMARILLO",
                'vc_marca'=>"Kenworth",
                'vc_tipoDeVehiculo'=>"Publico",
            ]
            ]);
    }
}
