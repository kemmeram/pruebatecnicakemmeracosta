<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblUsuarioVehiculoPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_usuario_vehiculo_pivot', function (Blueprint $table) {
            $table->integer('i_fk_id_users')->unsigned();
            $table->foreign('i_fk_id_users')->references('i_pk_id')->on('users');
            $table->integer('i_fk_id_vehiculo')->unsigned();
            $table->foreign('i_fk_id_vehiculo')->references('i_pk_id')->on('tbl_vehiculo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_usuario_vehiculo_pivot');
    }
}
