<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblVehiculoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_vehiculo', function (Blueprint $table) {
            $table->increments('i_pk_id');
            $table->string('vc_placa');
            $table->string('vc_color');
            $table->string('vc_marca');
            $table->string('vc_tipoDeVehiculo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_vehiculo');
    }
}
