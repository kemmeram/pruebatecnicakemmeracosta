<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('i_pk_id');
            $table->string('vc_numeroCedula');
            $table->string('name');
            $table->string('vc_segundoNombre');
            $table->string('vc_apellidos');
            $table->string('vc_direccion');
            $table->string('vc_telefono');
            $table->string('vc_ciudad');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('i_fk_id_tipo')->unsigned();
            $table->foreign('i_fk_id_tipo')->references('i_pk_id')->on('tbl_tipo');     
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
