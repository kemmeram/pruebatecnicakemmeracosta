$(function()
{
	var URL = $('#registrarvehiculo').data('url');

    $('#formRegistroVehiculo').on('submit', function(e){
        
        $.post(
            URL+'/registro_vehiculo',
            $(this).serialize(),
            function(data){
               
                    	    if(data.status == 'error')
							{
								validador(data.errors);
								var listaError='';
								var num=1;
								$.each(data.errors, function(i, e){
					              listaError += '<li class="list-group-item text-danger">'+num+'. '+e+'</li>';
					              num++;
					            });
								$('#list_error').html(listaError);
								
								
							} 
							else 
							{
								validador(data.errors);
								 swal("Bien hecho!", "Vehículo registrado con exito!", "success")
								 setTimeout(function(){
									document.getElementById("formRegistroVehiculo").reset();
									$('#registroVehiculoModal').modal('hide');
									location.reload();
								},1500)
								
							}

                    }
                );
        e.preventDefault();
	});
	

	var validador = function(data)
	{

		$('#formRegistroVehiculo .form-group').removeClass('has-error');
		var selector = '';
		for (var error in data){
		    if (typeof data[error] !== 'function') {
		        switch(error)
		        {

		        	case 'txt_placa':
		        	case 'txt_color':
		        	case 'txt_marca':
		        	case 'txt_tipoVeh':
		        		selector = 'input';
		        	break;

		        	case 'selectPropietarios':
		        	case 'selectConductores':
		        		selector = 'select';
		        	break;

		        }
		        $('#formRegistroVehiculo '+selector+'[name="'+error+'"]').closest('.form-group').addClass('has-error');
		    }
		}
	}

	function Solo_Numerico(variable)
	{
    	Numer=parseInt(variable);
        if (isNaN(Numer))
        {
            return "";
        }
        return Numer;
    }
    function ValNumero(Control)
    {
        Control.value=Solo_Numerico(Control.value);
    }




});