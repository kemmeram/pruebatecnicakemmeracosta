$(function()
{
	var URL = $('#registrarusuario').data('url');

    $('#formRegistroUsuario').on('submit', function(e){
        
        $.post(
            URL+'/registro',
            $(this).serialize(),
            function(data){
               
                    	    if(data.status == 'error')
							{
								validador(data.errors);
								var listaError='';
								var num=1;
								$.each(data.errors, function(i, e){
					              listaError += '<li class="list-group-item text-danger">'+num+'. '+e+'</li>';
					              num++;
					            });
								$('#list_error').html(listaError);
								
								
							} 
							else 
							{
								validador(data.errors);
								 swal("Bien hecho!", "Usuario registrado con exito!", "success")
								 setTimeout(function(){
				                      document.getElementById("formRegistroUsuario").reset();
                                      $('#registroUsuarioModal').modal('hide');
                                      location.reload();
				                  },1500)
								
							}

                    }
                );
        e.preventDefault();
	});
	

	var validador = function(data)
	{

		$('#formRegistroUsuario .form-group').removeClass('has-error');
		var selector = '';
		for (var error in data){
		    if (typeof data[error] !== 'function') {
		        switch(error)
		        {

		        	case 'numCedula':
		        	case 'apellidos':
		        	case 'mail':
		        	case 'telefono':
		        	case 'celular':
		        	case 'celular':
		        		selector = 'input';
		        	break;

		        	case 'cades':
		        	case 'tipo_documento':
		        	case 'genero':
		        	case 'localidad':
		        	case 'estrato':
		        	case 'motivo_consulta':
		        	case 'pregunta_1':
		        	case 'pregunta_2':
		        	case 'pregunta_3':
		        		selector = 'select';
		        	break;


		        	case 'cades':
		        		selector = 'radio';
		        	break;
		        }
		        $('#formRegistroUsuario '+selector+'[name="'+error+'"]').closest('.form-group').addClass('has-error');
		    }
		}
	}

	function Solo_Numerico(variable)
	{
    	Numer=parseInt(variable);
        if (isNaN(Numer))
        {
            return "";
        }
        return Numer;
    }
    function ValNumero(Control)
    {
        Control.value=Solo_Numerico(Control.value);
    }




});