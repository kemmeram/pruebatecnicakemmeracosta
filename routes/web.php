<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix' => '/'], function()
{
    Route::get('/', function () {
        return view('index');
    });
    
});

Route::group(['prefix' => 'usuario'], function()
{
    $controller = "\\App\\Modulos\\Usuarios\\Controllers\\";

    Route::get('/index', [
        'uses' => $controller.'UsuarioController@index',
        'as' => 'index_usuario'
    ]);

    Route::post('/registro', [
        'uses' => $controller.'UsuarioController@crear',
        'as' => 'registro_usuario'
    ]);
});

Route::group(['prefix' => 'vehiculo'], function()
{
    $controller = "\\App\\Modulos\\Vehiculo\\Controllers\\";

    Route::get('/index_vehiculo', [
        'uses' => $controller.'VehiculoController@index',
        'as' => 'index_vehiculo'
    ]);

    Route::post('/registro_vehiculo', [
        'uses' => $controller.'VehiculoController@create_vehiculo',
        'as' => 'registro_vehiculo'
    ]);
});

Route::group(['prefix' => 'informe'], function()
{
    $controller = "\\App\\Modulos\\Reportes\\Controllers\\";

    Route::get('/informe', [
        'uses' => $controller.'InformeController@index',
        'as' => 'informe'
    ]);
});