@extends('master')


@section('content')
<div class="col-md-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <h2 text-center text-mute><u>Sistema de información para la empresa de transpote ACME S.A</u></h2> 
    </div>
  </div>  
</div>
    <div class="col-md-12">
        <div id="carouselIndex" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img class="d-block w-100" src="/images/first.jpg" alt="First slide">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="/images/second.jpg" alt="Second slide">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="/images/third.jpg" alt="Third slide">
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselIndex" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselIndex" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>

    </div>
@endsection