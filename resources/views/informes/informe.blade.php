@extends('master')


@section('content')
    <div class="col-md-12">
        
            <div class="row">
                <div class="col-md-12">
                    <div class="caption-desc font-grey-cascade"> 
                        INFORME<br><br>
                        
                    </div>
                </div>
                <div class="col-md-12">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="data-filter">
                        <thead class="flip-content">
                        <tr>
                            <th class=""> # </th>
                            <th class=""> Placa </th>
                            <th class=""> Marca </th>
                            <th class=""> Propietario </th>
                            <th class=""> Conductor </th>
                          
                        </tr>
                        </thead>
                        <tbody>

                            @forelse($datos as $dato)
                        
                                <tr class="">
                                    <td>{{ $dato['id'] }}</td>
                                    <td>{{ $dato['placa'] }}</td>
                                    <td>{{ $dato['marca'] }}</td>
                                    <td>
                                        {{$dato['propietario']["FullName"]}}
                                    </td>  
                                    <td>
                                        @forelse ($dato['conductores'] as $conductor)
                                            
                                            <li> {{$conductor["FullName"]}}  </li> 
                                            
                                        @empty
                                            No asignado
                                        @endforelse
                                          
                                        
                                    </td>                                
                                </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                    <div class="modal fade" id="registroVehiculoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Gestión de Vehiculos</h5>
                             
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                                <form id="formRegistroVehiculo" method="post">
                                    <input type="hidden" value = 0 id="id" name="id">
                                      @csrf
                                      <div class="form-group">
                                        <label for="lb_placa">Placa</label>
                                        <input type="text" class="form-control" id="txt_placa" name="txt_placa">
                                      </div>
                                      <div class="form-group">
                                          <label for="lb_color">Color</label>
                                          <input type="text" class="form-control" id="txt_color" name="txt_color">
                                      </div>
                                      <div class="form-group">
                                        <label for="lb_marca">Marca</label>
                                        <input type="text" class="form-control" id="txt_marca" name="txt_marca">
                                      </div>
                                      <div class="form-group">
                                        <label for="lb_tipoVeh">Tipo de Vehículo</label>
                                        <input type="text" class="form-control" id="txt_tipoVeh" name="txt_tipoVeh">
                                      </div>
                                      
                                    
                            </div>
                            <div class="modal-footer">
                                <ul class="list-group" id="list_error"> 
                                </ul>
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                              <button type="submit" class="btn btn-primary">Guardar</button>
                            </div>
                          </div>
                        </div>
                        </form>
                      </div>
                </div>
            </div>

    </div>
@endsection