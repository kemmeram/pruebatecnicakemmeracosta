@extends('master')

@section('script')
  @parent

    <script src="{{ asset('js/vehiculo.js') }}"></script> 
@stop

@section('content')
    <div class="col-md-12" id="registrarvehiculo" data-url="{{url('/vehiculo')}}">
        
            <div class="row">
                <div class="col-md-12">
                    <br><br>
                    <h3>Gestión de Vehículos</h3>
                    <p>La siguiente tabla lista los vehículos registrados en el sistema</p>
                </div>
                <div class="col-md-12">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#registroVehiculoModal">
                        Registrar Vehículo
                    </button>
                    <br><br>
                </div>
                <div class="col-md-12">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="data-filter">
                        <thead class="flip-content">
                        <tr>
                            <th class=""> # </th>
                            <th class=""> Placa </th>
                            <th class=""> Color </th>
                            <th class=""> Marca </th>
                            <th class=""> Tipo de Vehículo </th>
                          
                        </tr>
                        </thead>
                        <tbody>
                            @forelse($vehiculos as $vehiculo)
                                <tr class="">
                                    <td>{{ $vehiculo['i_pk_id'] }}</td>
                                    <td>{{ $vehiculo['vc_placa'] }}</td>
                                    <td>{{ $vehiculo['vc_color'] }}</td>
                                    <td>{{ $vehiculo['vc_marca'] }}</td>
                                    <td>{{ $vehiculo['vc_tipoDeVehiculo'] }}</td>                                
                                </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                    <div class="modal fade" id="registroVehiculoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Gestión de Vehiculos</h5>
                             
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                                <form id="formRegistroVehiculo" method="post">
                                    <input type="hidden" value = 0 id="id" name="id">
                                      @csrf
                                      <div class="form-group">
                                        <label for="lb_placa">Placa</label>
                                        <input type="text" class="form-control" id="txt_placa" name="txt_placa">
                                      </div>
                                      <div class="form-group">
                                          <label for="lb_color">Color</label>
                                          <input type="text" class="form-control" id="txt_color" name="txt_color">
                                      </div>
                                      <div class="form-group">
                                        <label for="lb_marca">Marca</label>
                                        <input type="text" class="form-control" id="txt_marca" name="txt_marca">
                                      </div>
                                      <div class="form-group">
                                        <label for="lb_tipoVeh">Tipo de Vehículo</label>
                                        <select class="selectpicker form-control" id="txt_tipoVeh" name="txt_tipoVeh">
                                          <option value=" " >Seleccionar</option>
                                          <option value="Particular" >Particular</option>
                                          <option value="Publico" >Publico</option>
                                        </select>
                                      </div>
                                      
                                      <div class="form-group">
                                        <label for="tipo">Propietario</label>
                                        <select class="selectpicker form-control" id="selectPropietario" name="selectPropietarios">
                                            <option value=" " >Seleccionar</option>
                                            @forelse($propietarios as $propietario)
                                            <option value="{{$propietario["i_pk_id"]}}">{{$propietario["FullName"]}}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                      </div>
                                      <div class="form-group">
                                        <label for="tipo">Conductores</label>
                                        <select class="selectpicker form-control" multiple name="selectConductores[]">
                                          @forelse($conductores as $conductor)
                                            <option value="{{$conductor["i_pk_id"]}}">{{$conductor["FullName"]}}</option>
                                          @empty
                                          @endforelse
                                        </select>
                                        
                                      </div>
                                      
                                    
                            </div>
                            <div class="modal-footer">
                                <ul class="list-group" id="list_error"> 
                                </ul>
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                              <button type="submit" class="btn btn-primary">Guardar</button>
                            </div>
                          </div>
                        </div>
                        </form>
                      </div>
                </div>
            </div>

    </div>
@endsection

@push('functions')
<script>
    $(document).ready( function () {
        $('#data-filter').DataTable({
            lengthMenu: [
                [5, 10, 25, 50, 100, -1],
                [5, 10, 25, 50, 100, "Todo"]
            ],
            responsive: true,
            colReorder: true,
           
            buttons: [
                { extend: 'excel', className: 'btn btn-circle btn-icon-only btn-default tooltips t-excel', text: '<i class="fa fa-file-excel-o"></i>',},
                { extend: 'csv', className: 'btn btn-circle btn-icon-only btn-default tooltips t-csv',  text: '<i class="fa fa-file-text-o"></i>', },
                { extend: 'colvis', className: 'btn btn-circle btn-icon-only btn-default tooltips t-colvis', text: '<i class="fa fa-bars"></i>'},
            ],
            pageLength: 10,
            dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        });
    } );
</script>
@endpush

