@extends('master')

@section('script')
  @parent

    <script src="{{ asset('js/usuario.js') }}"></script> 
@stop

@section('content')
    <div class="col-md-12" id="registrarusuario" data-url="{{url('/usuario')}}">
        
            <div class="row">
                <div class="col-md-12">
                    <br><br>
                    <h3>Gestión de usuarios</h3>
                    <p>La siguiente tabla lista los usuarios registrados en el sistema</p>
                </div>
                <div class="col-md-12">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#registroUsuarioModal">
                        Registrar Usuario
                      </button>
                    <br><br>
                </div>
                <div class="col-md-12">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="data-filter">
                        <thead class="flip-content">
                        <tr>
                            <th class=""> # </th>
                            <th class=""> Cedula </th>
                            <th class=""> Nombre </th>
                            <th class=""> Segundo Nombre </th>
                            <th class=""> Apellido </th>
                            <th class=""> Dirección </th>
                            <th class=""> Telefono </th>
                            <th class=""> Ciudad </th>
                        </tr>
                        </thead>
                        <tbody>
                            @forelse($usuarios as $usuario)
                                <tr class="">
                                    <td>{{ $usuario['i_pk_id'] }}</td>
                                    <td>{{ $usuario['vc_numeroCedula'] }}</td>
                                    <td>{{ $usuario['name'] }}</td>
                                    <td>{{ $usuario['vc_segundoNombre'] }}</td>
                                    <td>{{ $usuario['vc_apellidos'] }}</td>
                                    <td>{{ $usuario['vc_direccion'] }}</td>
                                    <td>{{ $usuario['vc_telefono'] }}</td>
                                    <td>{{ $usuario['vc_ciudad'] }}</td>
                                
                                </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>

                    <div class="modal fade" id="registroUsuarioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="lb_modal">Gestión de Usuario</h5>
                             
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                                <form id="formRegistroUsuario" method="post">
                                    <input type="hidden" value = 0 id="id" name="id">
                                      @csrf
                                      <div class="form-group">
                                        <label for="cedula">Numero de Cedula</label>
                                        <input type="number" class="form-control" id="numCedula" name="numCedula">
                                      </div>
                                      <div class="form-group">
                                          <label for="pNombre">Primer Nombre</label>
                                          <input type="text" class="form-control" id="fNombre" name="fNombre">
                                      </div>
                                      <div class="form-group">
                                        <label for="sNombre">Segundo Nombre</label>
                                        <input type="text" class="form-control" id="tNombre" name="tNombre">
                                      </div>
                                      <div class="form-group">
                                        <label for="apellido">Apellidos</label>
                                        <input type="text" class="form-control" id="inp_apellidos" name="inp_apellidos">
                                      </div>
                                      <div class="form-group">
                                        <label for="direccion">Dirección</label>
                                        <input type="text" class="form-control" id="direc" name="direc">
                                      </div>
                                      <div class="form-group">
                                        <label for="telefono">Telefono</label>
                                        <input type="text" class="form-control" id="phone" name="phone">
                                      </div>
                                      <div class="form-group">
                                        <label for="ciudad">Ciudad</label>
                                        <input type="text" class="form-control" id="city" name="city">
                                      </div>
                                      <div class="form-group">
                                        <label for="mail">Mail</label>
                                        <input type="email" class="form-control" id="mail" name="mail">
                                      </div>
                                      <div class="form-group">
                                        <label for="tipo">Tipo de Usuario</label>
                                        <select class="form-control" id="selectTipo" name="selectTipo">
                                            <option value=" " >Seleccionar</option>
                                            @forelse($tipos as $tipo)
                                            <option value="{{$tipo->i_pk_id}}">{{$tipo->vc_tipo}}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                      </div>
                                    
                            </div>
                            <div class="modal-footer">
                                <ul class="list-group" id="list_error"> 
                                </ul>
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                              <button type="submit" class="btn btn-primary">Guardar</button>
                            </div>
                          </div>
                        </div>
                        </form>
                      </div>

                </div>
            </div>

    </div>
@endsection

@push('functions')
<script>
    $(document).ready( function () {
        $('#data-filter').DataTable({
            lengthMenu: [
                [5, 10, 25, 50, 100, -1],
                [5, 10, 25, 50, 100, "Todo"]
            ],
            responsive: true,
            colReorder: true,
           
            buttons: [
                { extend: 'excel', className: 'btn btn-circle btn-icon-only btn-default tooltips t-excel', text: '<i class="fa fa-file-excel-o"></i>',},
                { extend: 'csv', className: 'btn btn-circle btn-icon-only btn-default tooltips t-csv',  text: '<i class="fa fa-file-text-o"></i>', },
                { extend: 'colvis', className: 'btn btn-circle btn-icon-only btn-default tooltips t-colvis', text: '<i class="fa fa-bars"></i>'},
            ],
            pageLength: 10,
            dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        });
    } );
</script>
@endpush